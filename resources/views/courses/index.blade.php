@extends('app')

@section('content')
	<h1>Courses</h1>
	<hr>
	<a href="/courses/create">Create</a>
	@foreach ($courses as $course)
		<course>
			<h2><a href="/courses/{{ $course->id }}/edit">{{ $course->name }}</a></h2>
			<div class="address">{{	$course->address }}</div>
			<div class="link">{{ $course->link }}</div>
			<div class="phone">{{ $course->phone }}</div>
		</course>
	@endforeach
@stop