@extends('app')

@section('content')
	<h1>Edit Course</h1>

	<hr>

	@include ('errors.list')

	{!! Form::model($course, ['method' => 'PATCH', 'url' => 'courses/' . $course->id ]) !!}

		@include ('courses.form',['submitButtonText' => 'Update Course Information'])

	{!! Form::close() !!}

@stop