<div class="form-group">
	{!! Form::label('name', 'Name:') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('address', 'Address:') !!}
	{!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('link', 'Website:') !!}
	{!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('phone', 'Phone:') !!}
	{!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>
<div class="teebox">
	<div class="teebox-name">Teebox #1</div>
	{!! Form::hidden('teebox[0][id]', !empty($teeboxes[0]) ? $teeboxes[0]->id : null) !!}
	<div class="form-group">
		{!! Form::label('teebox[0][color]', 'Color') !!}
		{!! Form::text('teebox[0][color]', !empty($teeboxes[0]) ? $teeboxes[0]->color : null, ['class' => 'form-control']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('teebox[0][rating]', 'Course Rating') !!}
		{!! Form::text('teebox[0][rating]', !empty($teeboxes[0]) ? $teeboxes[0]->rating : null, ['class' => 'form-control']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('teebox[0][slope]', 'Slope') !!}
		{!! Form::text('teebox[0][slope]', !empty($teeboxes[0]) ? $teeboxes[0]->slope : null, ['class' => 'form-control']) !!}
	</div>
</div>
<div class="teebox">
	<div class="teebox-name">Teebox #2</div>
	{!! Form::hidden('teebox[1][id]', !empty($teeboxes[1]) ? $teeboxes[1]->id : null) !!}
	<div class="form-group">
		{!! Form::label('teebox[1][color]', 'Color') !!}
		{!! Form::text('teebox[1][color]', !empty($teeboxes[1]) ? $teeboxes[1]->color : null, ['class' => 'form-control']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('teebox[1][rating]', 'Course Rating') !!}
		{!! Form::text('teebox[1][rating]', !empty($teeboxes[1]) ? $teeboxes[1]->rating : null, ['class' => 'form-control']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('teebox[1][slope]', 'Slope') !!}
		{!! Form::text('teebox[1][slope]', !empty($teeboxes[1]) ? $teeboxes[1]->slope : null, ['class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>
