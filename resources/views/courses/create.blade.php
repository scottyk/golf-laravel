@extends('app')

@section('content')
	<h1>Add Course</h1>

	<hr>
	
	@include ('errors.list')

	{!! Form::open(['url' => 'courses']) !!}
	
		@include ('courses.form', ['submitButtonText' => 'Add Course Information'])

	{!! Form::close() !!}

@stop