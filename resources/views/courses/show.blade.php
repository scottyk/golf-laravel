@extends('app')

@section('content')
	<h1>Course</h1>

	<course>
		<h2>{{ $course->name }}</h2>
		<div>{{ $course->address }}</div>
		<div>{{ $course->link }}</div>
		<div>{{ $course->phone }}</div>
	</course>
@stop