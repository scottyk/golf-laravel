<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Teebox extends Model {

	protected $fillable = ['color', 'rating', 'slope'];

	public function course() {
		return $this->belongsTo('App\Course');
	}
}
