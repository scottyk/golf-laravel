<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model {

	protected $fillable = ['name', 'address', 'link', 'phone'];

	public function teeboxes() {
		return $this->hasMany('App\Teebox');
	}
}
