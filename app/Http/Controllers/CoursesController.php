<?php namespace App\Http\Controllers;

use App\Course;
use App\Teebox;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CourseRequest;

class CoursesController extends Controller {

	public function index() {
		$courses = Course::orderBy('name')->get();

		return view('courses.index', compact('courses'));
	}

	public function show($id) {
		$course = Course::findOrFail($id);

		return view('courses.show', compact('course'));
	}

	public function create() {
		return view('courses.create');
	}
	
	public function store(CourseRequest $request) {
		Course::create($request->all());
		
		return redirect('courses');
	}

	public function edit($id) {
		$course = Course::findOrFail($id);
		$teeboxes = $course->teeboxes;

		return view('courses.edit', compact('course', 'teeboxes'));
	}

	public function update($id, CourseRequest $request) {
		$course = Course::findOrFail($id);
		$course->update($request->all());

		for($i = 0; $i < 2; $i++) {
			if ($request->teebox[$i]['id']) {
				$teebox = Teebox::find($request->teebox[$i]['id']);
				$teebox->update($request->teebox[$i]);
			} else {
				$teebox = new Teebox($request->teebox[$i]);
				$teebox = $course->teeboxes()->save($teebox);
			}
		}		

		return redirect('courses');
	}
}
